
"""Fichier qui doit être exécuté par l'utilisateur"""
#Tous les imports nécessaires à la pipeline
from datetime import datetime
from ptd.importations.importjson import ImportJson
from ptd.importations.importcsv import ImportCSV
from ptd.modele.pipeline import Pipeline
from ptd.toutes_transfo.jointure import Jointure
from ptd.toutes_transfo.centrage import Centrage
from ptd.toutes_transfo.normalisation import Normalisation
from ptd.exportations.exportationcsv import ExportationCSV
from ptd.toutes_transfo.selection import Selection
from ptd.toutes_transfo.fenetrage import Fenetrage
from ptd.toutes_transfo.agregation import Agregation

#Bonjour, bienvenue sur l'application MJC.

# Création de la variable qui contient le chemin du document que l'utilisateur veut importer.
# Veuillez copier-coller votre chemin au fichier :
cheminImport1= "/2013-02.json.gz"
cheminImport2= "/synop.201302.csv.gz"

# Création de la table issu du document importé (commentez l'import inutile si besoin) :
table_import1 = ImportJson(cheminImport1)
table_import2 = ImportCSV(cheminImport2, ";")

# Création de la variable qui contient le nom du fichier dans lequel
# la table sera exportée (le fichier est exporté dans le dossier courant) :
nomExport1= "slurp.csv"
nomExport2 = "machin.csv"

# Création de la liste contenant les transformations (et leurs arguments)
# que souhaite faire l'utilisateur :
transformations1 = [Selection(['fields.consommation_brute_gaz_grtgaz','fields.consommation_brute_gaz_terega','fields.code_insee_region']),
                    Agregation('nationale', 'fields.code_insee_region', ['fields.consommation_brute_gaz_grtgaz','fields.consommation_brute_gaz_terega'], lambda x : sum(x)),
                    Centrage(['fields.consommation_brute_gaz_terega'])]
transformations2 = [Fenetrage('date', datetime(2013,2,28), datetime(2013,2,28), "%Y%m%d%H%M%S00"),
                    Normalisation(['ff','t']),
                    Jointure(ImportJson(cheminImport1).importer())]

#La pipeline est créé automatiquement ci-dessous. Veuillez lancer le code.
pipeline1 = Pipeline([table_import1, transformations1, ExportationCSV(nomExport1, ";")])
pipeline2 = Pipeline([table_import2, transformations2, ExportationCSV(nomExport2, ";")])
pipeline1.run()
pipeline2.run()
