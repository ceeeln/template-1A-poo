"""Module contenant la classe abstraite Exportation"""

#imports nécessaires :
from abc import ABC, abstractmethod

from ptd.operations import Operation

class Exportation(ABC,Operation):
    """
    Exportation d'un fichier

    Attributes
    ----------
    nom : str
       nom que l'on souhaite donner au fichier
    """

    def __init__(self, nom):
        super().__init__()
        self.nom = nom

    @abstractmethod
    def exporter(self, table):
        """Méthode abstaite dont hérite toutes les exportations suivantes"""
        pass
