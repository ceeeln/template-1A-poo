"""Module contenant la classe ExportationCSV"""

#imports nécessaires :
from ptd.exportations.exportation import Exportation
from ptd.modele.table import Table

class ExportationCSV(Exportation):
    """
    Exporter un fichier sous format CSV

    Attributes
    ----------
    nom : str
       nouveau nom que l'on souhaite donner au fichier (mettre .csv dans le nom)
    sep : str
       separateur
    """

    def __init__(self, nom, sep):
        self.sep = sep
        super().__init__(nom)

    def exporter(self, table):
        """
        Exporter un fichier sous format csv

        Notre body initial est une sous-liste de listes, chacune étant une colonne.
        Nous créons un nouveau body (qui est sa transposée) pour importer les données.

        Parameters
        ----------
        table : Table
           liste contenant les données à imputer dans le fichier csv
        """
        import csv
        header = table.get_header()
        #body : list[list]
        body = table.get_body()
        #Création d'un nouveau fichier pour écrire le fichier csv
        with open(self.nom, 'w', newline= '') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter= self.sep)
            csv_writer.writerow(header)
            nv_body=[]
            if body :
                for indice in range (len(body[0])):
                    ligne = []
                    #colonne = sous_liste
                    for colonne in body:
                        ligne.append(colonne[indice])
                    nv_body.append(ligne)
            csv_writer.writerows(nv_body)
        print("L'exportation a bien été faite dans le dossier courant")
