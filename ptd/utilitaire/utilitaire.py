"""Module contenant des fonctions nécessaires à d'autres classes mais qui sont statiques"""

#imports nécessaires
from math import sqrt
from collections.abc import MutableMapping

def flatten_dict(dico: MutableMapping, parent_key: str = '', sep: str ='.') -> MutableMapping:
    """Fonction permettant d' 'aplatir' un dictionnaire"""
    items = []
    for key, dico_imbrique in dico.items():
        new_key = parent_key + sep + key if parent_key else key
        if isinstance(dico_imbrique, MutableMapping):
            items.extend(flatten_dict(dico_imbrique, new_key, sep=sep).items())
        else:
            items.append((new_key, dico_imbrique))
    return dict(items)

def max_liste_dico(liste):
    """Fonction permettant d'avoir l'élément le plus long d'une liste ou d'un dictionnaire"""
    plus_long=liste[0]
    for element in liste :
        if len(element)>len(plus_long):
            plus_long=element
    return plus_long

def moyenne_ecart_type(liste):
    """Fonction permettant de calculer la moyenne et l'écart-type d'une colonne"""
    somme=0
    somme2=0
    nombre=0
    for elements in liste:
        if isinstance(elements, float) or isinstance(elements, int):
            somme+=elements
            somme2+=elements*elements
            nombre+=1
    if nombre==0 :
        print("Pas une colonne numérique")
        return 0, 1
    moyenne = somme/nombre
    moyenne2 = somme2/nombre
    return moyenne, sqrt(moyenne2-moyenne*moyenne)

def conversion(liste):
    """Fonction permettant de convertir une des str en float si besoin"""
    nv_liste = []
    for element in liste :
        try:
            float(element)
        except:
            nv_liste.append(element)
        else:
            nv_liste.append(float(element))
    return nv_liste
