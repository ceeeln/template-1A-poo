"""Module contenanat la classe Table"""

class Table:
    """
    Classe Table

    Cette classe permet d'unifier tous les types d'importation qu'on peut avoir
    (dictionnaire, listes de listes) pour pouvoir simplifier les codes des transformations.

    Attributes
    ----------
    data : object
        un tableau qui a été importé sur python

    Examples
    --------
    >>> table = Table([["slurp", "truc"],[7],[9]])
    >>> print(table)
    |slurp||truc|
    |    7||   9|
    >>> table.get_header()
    ['slurp', 'truc']
    >>> table.get_body()
    [[7], [9]]
    """

    def __init__(self,data):
        self.__header = data[0]
        self.__body = data[1:]

    def set_header(self, nvl_tete):
        """Méthode permettant de mettre à jour la tête d'une table"""
        self.__header = nvl_tete

    def set_body(self, nv_corps):
        """Méthode permettant de mettre à jour le corps d'une table"""
        self.__body = nv_corps

    def get_header(self):
        """Méthode permettant d'avoir la tête (noms des colonnes) d'une table"""
        return self.__header

    def get_body(self):
        """Méthode permettant d'avoir le corps d'une table"""
        return self.__body

    def __str__(self):
        tete=""
        corps=""
        liste_plus_long=[len(str(nom)) for nom in self.__header]
        if not self.__body and not self.__header :
            return "Tableau vide"
        for indice_col in range (len(self.__body)):
            for element in self.__body[indice_col]:
                if len(str(element))>liste_plus_long[indice_col]:
                    liste_plus_long[indice_col]=len(str(element))
        for nom in self.__header :
            tete+="|"+" "*((liste_plus_long[self.__header.index(nom)]-len(nom))//2)+str(nom) \
                 +" "*((liste_plus_long[self.__header.index(nom)]-len(nom))-(liste_plus_long[self.__header.index(nom)]-len(nom))//2)+"|"
        if self.__body:
            for indice_ligne in range (len(self.__body[0])-1):
                for indice_col in range (len(self.__body)):
                    corps+="|"+" "*(liste_plus_long[indice_col]-len(str(self.__body[indice_col][indice_ligne]))) \
                          +str(self.__body[indice_col][indice_ligne])+"|"
                corps+="\n"
        for indice_col in range (len(self.__body)):
            corps+="|"+" "*(liste_plus_long[indice_col]-len(str(self.__body[indice_col][-1])))+str(self.__body[indice_col][-1])+"|"
        return tete + "\n" + corps

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
