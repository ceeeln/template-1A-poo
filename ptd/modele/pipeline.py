"""Module contenant la classe pipeline"""

#imports nécessaires :

class Pipeline :
    """
    Classe permettant de faire la pipeline


    Attributes
    ----------
    operations : liste[Operation]
    """

    def __init__(self,operations):
        self.importation=operations[0]
        self.transformations=operations[1:-1][0]
        self.exportation=operations[-1]

    def run(self):
        """
        Méthode permettant d'appliquer la pipeline sur une table

        Parameters
        ----------
        chemin : str
            chemin où se trouve les données auxquelles où applique la pipeline
        """
        table=self.importation.importer()
        for transfo in self.transformations:
            table=transfo.appliquer(table)
        self.exportation.exporter(table)
