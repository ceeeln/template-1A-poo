"""Module contenant la classe Aggégation"""

#imports nécessaires :
from ptd.toutes_transfo.transfo import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.geographie import get_departement, get_region
from ptd.toutes_transfo.selection import Selection


class Agregation(Transformation):
    """
    Classe permettant de changer la granularité

    Attributes
    ----------
    granularité : str
        granularité à respecter : nationale, regionale ou communale
    nom_col_code_geo : str
        nom exact porté par le code geo dans la table
    var_à_agreger : type : list[str]
        liste des variables à agreger
    f_lambda : fonction
        fonction d'agrégation
    """

    def __init__(self,granularite,nom_col_code_geo,var_à_agreger,f_lambda):
        super().__init__()
        self.granularite=granularite
        self.nom_col_code_geo=nom_col_code_geo
        self.var_à_agreger=var_à_agreger
        self.f_lambda=f_lambda


    def appliquer(self, table):
        """
        Méthode permettant de changer la granularité d'une table sélectionnée.

        Les colonnes contenant des valeurs manquantes ne peuvent pas être agrégées.

        Parameters
        ----------
        table : Table
            table à modifier

        Returns
        -------
        table : Table

        Examples
        --------
        >>> table1=Table([["code insee","consommation"],["35","35","78","75"],[19,12,13,15]])
        >>> table2=Table([["code insee","consommation","machin"],["35170","35200","78500","75000"],["NA",3,12,15],[10,20,30,40]])
        >>> table_nationale=Agregation("nationale", "code insee",["consommation"],lambda x: sum(x)).appliquer(table1)
        >>> print(table_nationale)
        | pays ||consommation|
        |France||          59|
        >>> table_regionale=Agregation("regionale", "code insee",["consommation"],lambda x: sum(x)).appliquer(table1)
        >>> print(table_regionale)
        |   region    ||consommation|
        |     Bretagne||          31|
        |Île-de-France||          28|
        >>> table_dep_NA=Agregation("departementale", "code insee",["consommation","machin"],lambda x: sum(x)).appliquer(table2)
        Les colonnes ['consommation'] contiennent des NA, l'agrégation ne se fera pas sur ces colonnes.
        >>> print(table_dep_NA)
        |  departement  ||machin|
        |Ille-et-Vilaine||    30|
        |          Paris||    40|
        |       Yvelines||    30|
        """
        if not table.get_body():
            print("Attention, table n'ayant pas de ligne")
            return table
        liste_code_geo=Selection([self.nom_col_code_geo]).appliquer(table).get_body()[0]
        table=Selection(self.var_à_agreger).appliquer(table)
        new_header=[]
        new_body=[]
        table_res=Table([[]])
        #on stocke le nom des colonnes qui ont des NA.
        nom_NA = []
        for indice_col in range(len(table.get_header())):
            nbr_na=0
            for elements in table.get_body()[indice_col]:
                if not isinstance(elements,float) and not isinstance(elements,int):
                    nbr_na+=1
            if nbr_na!=0:
                nom_NA.append(table.get_header()[indice_col])
        if nom_NA:
            print("Les colonnes "+str(nom_NA)+" contiennent des NA," \
                  " l'agrégation ne se fera pas sur ces colonnes.")
        if len(nom_NA)==len(self.var_à_agreger):
            return table
        nv_var=[]
        for element in self.var_à_agreger:
            if element not in nom_NA:
                nv_var.append(element)
        table=Selection(nv_var).appliquer(table)

        if self.granularite.lower()=="nationale":
            new_header.append("pays")
            new_body.append(["France"])
            for i in range(len(table.get_header())):
                new_header.append(table.get_header()[i])
                new_body.append([self.f_lambda(table.get_body()[i])])

        if self.granularite.lower()=="departementale":
            departement=[]
            for i in liste_code_geo :
                departement.append(get_departement(i))
            new_header.append("departement")
            for i in range(len(table.get_header())):
                new_header.append(table.get_header()[i])
            #on stocke les départements distincts
            distinct_dep=sorted(list(set(departement))) #on transforme le set en liste pour pouvoir le trier et connaître l'ordre dans lequel apparaitront les dep pour la doctest
            new_body.append(distinct_dep)
            same_dep=[]
            #on met dans same_region les elements liées à la même région et on fait leur somme
            for i in range(len(table.get_body())): #on itère sur l'ensemble des colonnes
                colonne=[]
                for dep in distinct_dep:
                    for j in range(len(departement)): #on itére selon la lgr de la colonne
                        if departement[j]==dep : #iterer sur la liste dep
                            same_dep.append(table.get_body()[i][j])
                    colonne.append((self.f_lambda(same_dep)))
                    same_dep=[] #reinitialise la liste
                new_body.append(colonne)

        if self.granularite.lower()=="regionale":
            #on refait la même chose
            region=[]
            for i in liste_code_geo :
                region.append(get_region(i))
            new_header.append("region")
            for i in range(len(table.get_header())):
                new_header.append(table.get_header()[i])
            #on stocke les départements distincts
            distinct_region=sorted(list(set(region)))
            new_body.append(distinct_region)
            same_region=[]
            #on met dans same_region les elements liées à la même région et on fait leur somme
            for i in range(len(table.get_body())): #on itère sur l'ensemble des colonnes
                colonne=[]
                for reg in distinct_region:
                    for j in range(len(region)): #on itére selon la lgr de la colonne
                        if region[j]==reg:
                            same_region.append(table.get_body()[i][j])

                    colonne.append(self.f_lambda(same_region))
                    same_region=[] #reinitialise la liste
                new_body.append(colonne)

        table_res.set_header(new_header)
        table_res.set_body(new_body)
        return  table_res


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
