"""Module contenant la classe Selection"""

#imports nécessaires :
from ptd.toutes_transfo.transfo import Transformation
from ptd.modele.table import Table

class Selection(Transformation):
    """
    Sélection des données à garder

    Attributes
    ----------
    nom_de_col : list(str)
        liste de colonnes à sélectionner
    """

    def __init__(self,nom_de_cols):
        super().__init__()
        self.nom_de_cols = nom_de_cols

    def appliquer(self,table):
        """
        Sélectionne des colonnes en entrée

        Parameters
        ----------
        table : Table
           La table où on ira chercher les colonnes
        nom_de_cols : list[str]
           listes avec les noms de colonnes à garder

        Returns
        -------
        une nouvelle table avec les colonnes que l'on souhaite garder

        Examples
        --------
        >>> selection1=Selection(['machin'])
        >>> selection2=Selection(['machin','slurp','bidule'])
        >>> table1 = Table([['truc','machin'],[9,5,3],[15,0,8]])
        >>> table2 = Table([['slurp','machin'],[11,5,0],[15,0,8]])
        >>> table1_selectionnée = selection1.appliquer(table1)
        >>> table2_selectionnée = selection2.appliquer(table2)
        Attention, certaines colonnes sélectionnées n'existent pas
        >>> print(table1_selectionnée)
        |machin|
        |    15|
        |     0|
        |     8|
        >>> print(table2_selectionnée)
        |machin||slurp|
        |    15||   11|
        |     0||    5|
        |     8||    0|
        """
        tete = table.get_header()
        corps = table.get_body()
        new_table = Table([[]])
        new_body = []
        new_head = []
        nb_colonne = 0
        for nom in self.nom_de_cols:
            for i in range (len(tete)):
                if tete[i] == nom:
                    new_body.append(corps[i])
                    nb_colonne+=1
        if nb_colonne!=len(self.nom_de_cols):
            print("Attention, certaines colonnes sélectionnées n'existent pas")
        new_head=[nom for nom in self.nom_de_cols if nom in tete]
        new_table.set_body(new_body)
        new_table.set_header(new_head)
        return new_table


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
