"""Module contenant la classe Transformation"""

#imports nécessaires :
from abc import ABC, abstractmethod
from ptd.operations import Operation


class Transformation(ABC,Operation) :
    """Classe abstraite dont hérite toutes les transformations"""
    def __init__(self):
        super().__init__()

    @abstractmethod
    def appliquer(self, table):
        """Méthode abstraite permettant d'avoir la méthode dans les classes héritées"""
        pass
