"""Module contenant la classe Centrage"""

#imports nécessaires :
from ptd.toutes_transfo.transfo import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.utilitaire import moyenne_ecart_type, conversion

class Centrage(Transformation):
    """
    Classe permettant de centrer des colonnes

    Attributes
    ----------
    noms_de_cols : liste[str]
        les colonnes de table à centrer
    """
    def __init__(self,noms_de_col):
        super().__init__()
        self.noms_de_col=noms_de_col

    def appliquer(self, table : Table):
        """
        Méthode permettant de centrer des colonnes de table

        Parameters
        ----------
        table : Table
            la table à modifier

        Returns
        -------
        table : Table

        Examples
        --------
        >>> table1 = Table([['truc','machin'],[9,5,7],[15,0,4]])
        >>> table2 = Table([['slurp','machin'],[11,'mq',0],[0,0,0]])
        >>> centrage1 = Centrage(['machin'])
        >>> centrage2 = Centrage(['machin','slurp'])
        >>> table1_centrée = centrage1.appliquer(table1)
        >>> table2_centrée = centrage2.appliquer(table2)
        >>> print(table1_centrée)
        |truc||      machin      |
        |   9|| 8.666666666666668|
        |   5||-6.333333333333333|
        |   7||-2.333333333333333|
        >>> print(table2_centrée)
        |slurp||machin|
        |  5.5||   0.0|
        |   mq||   0.0|
        | -5.5||   0.0|
        """
        tete = table.get_header()
        liste_indices = [tete.index(nom) for nom in self.noms_de_col if nom in tete]
        corps = table.get_body()
        nv_body=corps
        if len(liste_indices)!=len(self.noms_de_col):
            print("Attention, certaines colonnes sélectionnées n'existent pas," \
                 "la normalisation ne se fait pas sur elles")
        for indice in liste_indices :
            corps[indice]=conversion(corps[indice])
            moyenne = moyenne_ecart_type(corps[indice])[0]
            colonne=[]
            for element in corps[indice]:
                if isinstance(element, float) or isinstance(element, int):
                    element_nv = element-moyenne
                    colonne.append(element_nv)
                else :
                    colonne.append(element)
            nv_body[indice]=colonne
        table.set_body(nv_body)
        return table


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
