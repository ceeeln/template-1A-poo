"""Module contenant la classe Normalisation"""

#imports nécessaires :
from ptd.toutes_transfo.transfo import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.utilitaire import moyenne_ecart_type, conversion

class Normalisation(Transformation):
    """
    Classe permettant de normaliser des colonnes sélectionées d'une table

    Attributes
    ----------
    nom_de_cols : list[str]
        liste des noms de colonnes à normaliser
    """

    def __init__(self,nom_de_cols):
        super().__init__()
        self.nom_de_cols = nom_de_cols

    def appliquer(self, table : Table):
        """
        Aplique une normalisation sur une liste de colonnes

        Parameters
        ----------
        table : Table
            la table à modifier

        Returns
        -------
        table : Table
        Examples
        --------
        >>> normalisation1=Normalisation(['machin','bidule'])
        >>> normalisation2=Normalisation(['machin','slurp'])
        >>> table1 = Table([['truc','machin'],[9,5,3],['15','mq','8']])
        >>> table2 = Table([['slurp','machin'],[11,5,0],[15,0,8]])
        >>> table1_normalisée = normalisation1.appliquer(table1)
        Attention, certaines colonnes sélectionnées n'existent pas,la normalisation ne se fait pas sur elles
        >>> table2_normalisée = normalisation2.appliquer(table2)
        >>> print(table1_normalisée)
        |truc||machin|
        |   9||   1.0|
        |   5||    mq|
        |   3||  -1.0|
        >>> print(table2_normalisée)
        |       slurp        ||      machin       |
        |  1.2601238383238722|| 1.1966422450849268|
        |-0.07412493166611006|| -1.251035074406969|
        | -1.1859989066577619||0.05439282932204208|
        """
        tete = table.get_header()
        liste_indices = [tete.index(nom) for nom in self.nom_de_cols if nom in tete]
        corps = table.get_body()
        nv_body=corps
        if len(liste_indices)!=len(self.nom_de_cols):
            print("Attention, certaines colonnes sélectionnées n'existent pas," \
                 "la normalisation ne se fait pas sur elles")
        for indice in liste_indices :
            corps[indice] = conversion(corps[indice])
            moyenne, ecart_type = moyenne_ecart_type(corps[indice])
            colonne=[]
            for element in corps[indice]:
                if isinstance(element, float) or isinstance(element, int):
                    element_nv = (element-moyenne)/ecart_type
                    colonne.append(element_nv)
                else :
                    colonne.append(element)
            nv_body[indice]=colonne
        table.set_body(nv_body)
        return table


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
