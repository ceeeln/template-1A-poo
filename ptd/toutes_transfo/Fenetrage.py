"""Module contenant la classe Fenetrage"""

#imports nécessaires :
from datetime import datetime
from ptd.toutes_transfo.transfo import Transformation
from ptd.modele.table import Table

class Fenetrage(Transformation):
    """ Sélection des données sur une période temporelle

    Attributes
    ------
    nom_col_date : str
        nom de la colonne qui contient la date
    date1 : datetime
        une des bornes du fenêtrage
    date2 : datetime
        une des bornes du fenêtrage
    format_date : str
        le format de la date (ex : '%Y-%m-%d' pour '2022-05-27')
    """

    def __init__(self,nom_col_date,date1,date2,format_date):
        super().__init__()
        self.nom_col_date=nom_col_date
        self.date1=date1
        self.date2=date2
        self.format_date=format_date

    def appliquer(self,table):
        """
        Méthode permettant d'appliquer le fenêtrage sur une table

        Parameters
        ----------
        table : Table
            la table à modifier

        Returns
        ---------
        table : Table

        Examples
        --------
        >>> depart=datetime(2025,10,10)
        >>> arrivee=datetime(2025,10,13)
        >>> fenetrage=Fenetrage('date',arrivee,depart,'%Y-%m-%d')
        >>> table1 = Table([['truc','date','bidule'],[9,12,5],
        ... ['2025-10-10','2025-11-10','2025-10-13'],[13,1,2]])
        >>> table = fenetrage.appliquer(table1)
        >>> print(table)
        |truc||   date   ||bidule|
        |   9||2025-10-10||    13|
        |   5||2025-10-13||     2|
        """
        debut=min(self.date1,self.date2)
        fin=max(self.date1,self.date2)
        #on stocke l'indice qui correspond à la colonne date
        i = table.get_header().index(self.nom_col_date)
        new_body=[]
        indice_lignes=[]
        #on stocke les indices des lignes à garder
        for j in range (len(table.get_body()[i])):
            if datetime.strptime(table.get_body()[i][j],self.format_date)>=debut and \
            datetime.strptime(table.get_body()[i][j],self.format_date)<=fin:
                indice_lignes.append(j)
        #on recupere les données qui correspondent aux indices gardés
        for nb_colonne in range (len(table.get_body())):
            colonne=[table.get_body()[nb_colonne][indice] for indice in indice_lignes]
            new_body.append(colonne)
        # si on était sûr que le tableau était ordonné : on selectionne juste les lignes
        # entre les deux dates
        # new_body.append(table.body[i][start:end] avec start et end les indices des dates)
        final_table=Table([[]])
        final_table.set_body(new_body)
        final_table.set_header(table.get_header())
        return final_table


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
