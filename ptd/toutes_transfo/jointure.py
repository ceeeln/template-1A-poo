"""Module contenant la classe Jointure"""

#imports nécessaires :
from ptd.toutes_transfo.transfo import Transformation
from ptd.modele.table import Table

class Jointure(Transformation) :
    """
    Joindre deux tables selon une colonne en commum à spécifier

    Attributes
    ----------
    autre_table : Table
        la 2e table à joindre
    nom_col_jointe : str
        nom de la colonne sur laquelle joindre si besoin
    """

    def __init__(self, autre_table, nom_col_jointe=None):
        super().__init__()
        self.autre_table = autre_table
        self.nom_col_jointe=nom_col_jointe

    def appliquer(self, table):
        """
        Méthode permettant la jointure

        Parameters
        ----------
        table : Table
            table à modifier

        Returns
        -------
        table : Table

        Examples
        --------
        >>> table1 = Table([['truc','machin'],[9,5,3],[15,0,8]])
        >>> table2 = Table([['slurp','machin'],[11,5,0],[15,0,8]])
        >>> table3 = Table([['slurp','bidule'],[11,5,0],[15,0,8]])
        >>> jointure1 = Jointure(table2, 'machin')
        >>> jointure2 = Jointure(table1)
        >>> table = jointure1.appliquer(table1)
        >>> print(table)
        |truc||machin||slurp|
        |   9||    15||   11|
        |   5||     0||    5|
        |   3||     8||    0|
        >>> table_test = jointure2.appliquer(table3)
        >>> print(table_test)
        |slurp||bidule||truc||machin|
        |   11||    15||   9||    15|
        |    5||     0||   5||     0|
        |    0||     8||   3||     8|
        """
        tete = table.get_header()
        corps = table.get_body()
        autre_tete = self.autre_table.get_header()
        autre_corps = self.autre_table.get_body()
        indice = False
        table_finale = Table([[]])
        if not autre_corps:
            return table
        if not corps:
            return self.autre_table
        if len(corps[0])!=len(autre_corps[0]):
            print("Attention, les tableaux n'ont pas le même nombre de lignes, la jointure n'a pas été faite")
            return table
        if self.nom_col_jointe is None :
            table_finale.set_header(tete+autre_tete)
            table_finale.set_body(corps+autre_corps)
            return table_finale
        for indice_col in range (len(autre_tete)):
            if autre_tete[indice_col]==self.nom_col_jointe:
                indice = indice_col
        if not indice or self.nom_col_jointe not in tete:
            print("Attention, pas de colonne jointe, la jointure n'a pas été effectuée")
            return table
        del autre_tete[indice]
        del autre_corps[indice]
        table_finale.set_header(tete + autre_tete)
        table_finale.set_body(corps + autre_corps)
        return table_finale


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
