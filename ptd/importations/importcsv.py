"""Import des modules"""

#imports nécessaires :
from ptd.importations.importation import Importation
from ptd.modele.table import Table

class ImportCSV(Importation):
    """
    Importer une table CSV

    Attributes
    ----------
    sep : str
       le separateur
    chemin : str
       chemin du fichier à importer
    """

    def __init__(self, chemin, sep):
        self.sep = sep
        super().__init__(chemin)

    def importer(self):
        """
        Import table CSV

        Parameters
        ----------
        chemin : str
           chemin du fichier (le dossier et le nom du fichier)
        sep : str
           separateur
        Returns
        -------
        Table
        """
        import gzip
        import csv
        data = []
        with gzip.open(self.chemin, mode='rt') as gzfile :
            synopreader = csv.reader(gzfile, delimiter=self.sep)
            for row in synopreader :
                data.append(row)
        table = [data[0]]
        for indice_colonne in range (len(data[0])):
            colonne=[]
            for ligne in data[1:]:
                colonne.append(ligne[indice_colonne])
            table.append(colonne)
        return Table(table)
