"""Module contenant la classe ImportJSON"""

#imports nécessaires :
from ptd.importations.importation import Importation
from ptd.utilitaire.utilitaire import flatten_dict, max_liste_dico
from ptd.modele.table import Table

class ImportJson(Importation):
    """
    Import fichier de type JSON

    Parameters
    ----------
    chemin  : str
       chemin du fichier (dossier et nom du fichier)

    Returns
    -------
    Table
    """

    def __init__(self,chemin):
        super().__init__(chemin)

    def importer(self):
        """Importer un ficher JSON
        """
        import gzip
        import json
        with gzip.open(self.chemin, mode='rt', encoding="utf-8") as gzfile :
            data = json.load(gzfile)
        data = [flatten_dict(dico) for dico in data]
        dico_premiere_ligne = max_liste_dico(data)
        premiere_ligne = []
        table = [premiere_ligne]
        for key in dico_premiere_ligne :
            premiere_ligne.append(key)
            colonne = []
            for dico in data :
                if key in dico :
                    colonne.append(dico[key])
                else :
                    colonne.append("NA")
            table.append(colonne)
        return Table(table)
