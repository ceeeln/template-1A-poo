"""Module contenant la classe Importation, dont hérite tous les types d'importation"""

#import nécessaire :
from abc import ABC, abstractmethod

from ptd.operations import Operation
class Importation(ABC,Operation):
    """
    Importation

    Attributes
    ----------
    chemin : str
       chemin du fichier
    """

    def __init__(self, chemin):
        super().__init__()
        self.chemin = chemin

    @abstractmethod
    def importer(self):
        """Méthode abstaite dont hérite toutes les exportations suivantes"""
        pass
    